﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Dependencies
    {
        public string ChangeId { get; set; }
        public string Type { get; set; }
        public string Dependency { get; set; }
        public string DependencyId { get; set; }

        public Changes Change { get; set; }
        public Changes DependencyNavigation { get; set; }
    }
}
