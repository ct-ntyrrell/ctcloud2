﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class SiteWebusers
    {
        public int SiteId { get; set; }
        public int WebUserId { get; set; }

        public Site Site { get; set; }
        public Webusers WebUser { get; set; }
    }
}
