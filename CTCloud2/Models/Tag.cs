﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Tag
    {
        public Tag()
        {
            NurseCall = new HashSet<NurseCall>();
        }

        public int SiteId { get; set; }
        public int TagId { get; set; }
        public string Name { get; set; }

        public Site Site { get; set; }
        public ICollection<NurseCall> NurseCall { get; set; }
    }
}
