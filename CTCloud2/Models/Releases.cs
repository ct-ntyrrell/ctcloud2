﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Releases
    {
        public float Version { get; set; }
        public DateTime InstalledAt { get; set; }
        public string InstallerName { get; set; }
        public string InstallerEmail { get; set; }
    }
}
