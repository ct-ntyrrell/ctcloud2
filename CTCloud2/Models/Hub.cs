﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;

namespace CTCloud2.Models
{
    public partial class Hub
    {
        public PhysicalAddress MacAddress { get; set; }
        public string Licence { get; set; }
        public int SiteId { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }

        public Site Site { get; set; }
        public NurseCallFileUpLoad NurseCallFileUpLoad { get; set; }
    }
}
