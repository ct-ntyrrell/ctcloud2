﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Report
    {
        public Report()
        {
            MailingListReport = new HashSet<MailingListReport>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public int ReportPeriodId { get; set; }
        public bool DemoLicense { get; set; }
        public bool ManagerRoleLiteLicense { get; set; }
        public bool? ManagerRoleProLicense { get; set; }
        public bool? Active { get; set; }
        public bool StaffRoleProLicense { get; set; }
        public bool StaffRoleLiteLicense { get; set; }
        public bool? AdminLicense { get; set; }
        public string ReportName { get; set; }
        public int ReportTypeId { get; set; }

        public ReportPeriod ReportPeriod { get; set; }
        public ReportType ReportType { get; set; }
        public ICollection<MailingListReport> MailingListReport { get; set; }
    }
}
