﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Caller
    {
        public Caller()
        {
            NurseCall = new HashSet<NurseCall>();
        }

        public int SiteId { get; set; }
        public int CallerId { get; set; }
        public string Name { get; set; }
        public int HouseCodeId { get; set; }

        public HouseCode HouseCode { get; set; }
        public Site Site { get; set; }
        public ICollection<NurseCall> NurseCall { get; set; }
    }
}
