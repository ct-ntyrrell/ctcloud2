﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class CallType
    {
        public CallType()
        {
            NurseCall = new HashSet<NurseCall>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<NurseCall> NurseCall { get; set; }
    }
}
