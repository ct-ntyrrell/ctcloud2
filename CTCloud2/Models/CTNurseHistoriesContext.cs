﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CTCloud2.Models
{
    public partial class CTNurseHistoriesContext : DbContext
    {
        public CTNurseHistoriesContext()
        {
        }

        public CTNurseHistoriesContext(DbContextOptions<CTNurseHistoriesContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccountPeriod> AccountPeriod { get; set; }
        public virtual DbSet<Caller> Caller { get; set; }
        public virtual DbSet<CallType> CallType { get; set; }
        public virtual DbSet<Changes> Changes { get; set; }
        public virtual DbSet<Dependencies> Dependencies { get; set; }
        public virtual DbSet<Events> Events { get; set; }
        public virtual DbSet<HouseCode> HouseCode { get; set; }
        public virtual DbSet<Hub> Hub { get; set; }
        public virtual DbSet<MailingList> MailingList { get; set; }
        public virtual DbSet<MailingListReport> MailingListReport { get; set; }
        public virtual DbSet<Manager> Manager { get; set; }
        public virtual DbSet<NurseCall> NurseCall { get; set; }
        public virtual DbSet<Projects> Projects { get; set; }
        public virtual DbSet<Releases> Releases { get; set; }
        public virtual DbSet<Report> Report { get; set; }
        public virtual DbSet<ReportPeriod> ReportPeriod { get; set; }
        public virtual DbSet<ReportType> ReportType { get; set; }
        public virtual DbSet<ShiftPattern> ShiftPattern { get; set; }
        public virtual DbSet<Site> Site { get; set; }
        public virtual DbSet<SiteWebusers> SiteWebusers { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<Tags> Tags { get; set; }
        public virtual DbSet<Tenant> Tenant { get; set; }
        public virtual DbSet<WeblicenseType> WeblicenseType { get; set; }
        public virtual DbSet<WebroleType> WebroleType { get; set; }
        public virtual DbSet<Webusers> Webusers { get; set; }
        public virtual DbSet<Zone> Zone { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("multicorn");

            modelBuilder.Entity<AccountPeriod>(entity =>
            {
                entity.HasKey(e => new { e.PeriodId, e.TenantId });

                entity.ToTable("account_period");

                entity.Property(e => e.PeriodId).HasColumnName("period_id");

                entity.Property(e => e.TenantId).HasColumnName("tenant_id");

                entity.Property(e => e.Deadline)
                    .HasColumnName("deadline")
                    .HasColumnType("date")
                    .HasDefaultValueSql("('now'::text)::date");

                entity.Property(e => e.Ends)
                    .HasColumnName("ends")
                    .HasColumnType("date")
                    .HasDefaultValueSql("('now'::text)::date");

                entity.Property(e => e.Starts)
                    .HasColumnName("starts")
                    .HasColumnType("date")
                    .HasDefaultValueSql("('now'::text)::date");

                entity.HasOne(d => d.Tenant)
                    .WithMany(p => p.AccountPeriod)
                    .HasForeignKey(d => d.TenantId)
                    .HasConstraintName("account_period_tenant_id_fkey");
            });

            modelBuilder.Entity<Caller>(entity =>
            {
                entity.HasKey(e => new { e.SiteId, e.CallerId, e.HouseCodeId });

                entity.ToTable("caller");

                entity.Property(e => e.SiteId).HasColumnName("site_id");

                entity.Property(e => e.CallerId).HasColumnName("caller_id");

                entity.Property(e => e.HouseCodeId).HasColumnName("house_code_id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.HasOne(d => d.Site)
                    .WithMany(p => p.Caller)
                    .HasForeignKey(d => d.SiteId)
                    .HasConstraintName("caller_site_id_fkey");

                entity.HasOne(d => d.HouseCode)
                    .WithMany(p => p.Caller)
                    .HasForeignKey(d => new { d.SiteId, d.HouseCodeId })
                    .HasConstraintName("caller_site_id_house_code_id_fkey");
            });

            modelBuilder.Entity<CallType>(entity =>
            {
                entity.ToTable("call_type");

                entity.HasIndex(e => e.Name)
                    .HasName("call_type_name_key")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<Changes>(entity =>
            {
                entity.HasKey(e => e.ChangeId);

                entity.ToTable("changes", "sqitch");

                entity.ForNpgsqlHasComment("Tracks the changes currently deployed to the database.");

                entity.HasIndex(e => new { e.Project, e.ScriptHash })
                    .HasName("changes_project_script_hash_key")
                    .IsUnique();

                entity.Property(e => e.ChangeId)
                    .HasColumnName("change_id")
                    .ValueGeneratedNever()
                    .ForNpgsqlHasComment("Change primary key.");

                entity.Property(e => e.Change)
                    .IsRequired()
                    .HasColumnName("change")
                    .ForNpgsqlHasComment("Name of a deployed change.");

                entity.Property(e => e.CommittedAt)
                    .HasColumnName("committed_at")
                    .HasColumnType("timestamp with time zone")
                    .HasDefaultValueSql("clock_timestamp()")
                    .ForNpgsqlHasComment("Date the change was deployed.");

                entity.Property(e => e.CommitterEmail)
                    .IsRequired()
                    .HasColumnName("committer_email")
                    .ForNpgsqlHasComment("Email address of the user who deployed the change.");

                entity.Property(e => e.CommitterName)
                    .IsRequired()
                    .HasColumnName("committer_name")
                    .ForNpgsqlHasComment("Name of the user who deployed the change.");

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnName("note")
                    .HasDefaultValueSql("''::text")
                    .ForNpgsqlHasComment("Description of the change.");

                entity.Property(e => e.PlannedAt)
                    .HasColumnName("planned_at")
                    .HasColumnType("timestamp with time zone")
                    .ForNpgsqlHasComment("Date the change was added to the plan.");

                entity.Property(e => e.PlannerEmail)
                    .IsRequired()
                    .HasColumnName("planner_email")
                    .ForNpgsqlHasComment("Email address of the user who planned the change.");

                entity.Property(e => e.PlannerName)
                    .IsRequired()
                    .HasColumnName("planner_name")
                    .ForNpgsqlHasComment("Name of the user who planed the change.");

                entity.Property(e => e.Project)
                    .IsRequired()
                    .HasColumnName("project")
                    .ForNpgsqlHasComment("Name of the Sqitch project to which the change belongs.");

                entity.Property(e => e.ScriptHash)
                    .HasColumnName("script_hash")
                    .ForNpgsqlHasComment("Deploy script SHA-1 hash.");

                entity.HasOne(d => d.ProjectNavigation)
                    .WithMany(p => p.Changes)
                    .HasForeignKey(d => d.Project)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("changes_project_fkey");
            });

            modelBuilder.Entity<Dependencies>(entity =>
            {
                entity.HasKey(e => new { e.ChangeId, e.Dependency });

                entity.ToTable("dependencies", "sqitch");

                entity.ForNpgsqlHasComment("Tracks the currently satisfied dependencies.");

                entity.Property(e => e.ChangeId)
                    .HasColumnName("change_id")
                    .ForNpgsqlHasComment("ID of the depending change.");

                entity.Property(e => e.Dependency)
                    .HasColumnName("dependency")
                    .ForNpgsqlHasComment("Dependency name.");

                entity.Property(e => e.DependencyId)
                    .HasColumnName("dependency_id")
                    .ForNpgsqlHasComment("Change ID the dependency resolves to.");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .ForNpgsqlHasComment("Type of dependency.");

                entity.HasOne(d => d.Change)
                    .WithMany(p => p.DependenciesChange)
                    .HasForeignKey(d => d.ChangeId)
                    .HasConstraintName("dependencies_change_id_fkey");

                entity.HasOne(d => d.DependencyNavigation)
                    .WithMany(p => p.DependenciesDependencyNavigation)
                    .HasForeignKey(d => d.DependencyId)
                    .HasConstraintName("dependencies_dependency_id_fkey");
            });

            modelBuilder.Entity<Events>(entity =>
            {
                entity.HasKey(e => new { e.ChangeId, e.CommittedAt });

                entity.ToTable("events", "sqitch");

                entity.ForNpgsqlHasComment("Contains full history of all deployment events.");

                entity.Property(e => e.ChangeId)
                    .HasColumnName("change_id")
                    .ForNpgsqlHasComment("Change ID.");

                entity.Property(e => e.CommittedAt)
                    .HasColumnName("committed_at")
                    .HasColumnType("timestamp with time zone")
                    .HasDefaultValueSql("clock_timestamp()")
                    .ForNpgsqlHasComment("Date the event was committed.");

                entity.Property(e => e.Change)
                    .IsRequired()
                    .HasColumnName("change")
                    .ForNpgsqlHasComment("Change name.");

                entity.Property(e => e.CommitterEmail)
                    .IsRequired()
                    .HasColumnName("committer_email")
                    .ForNpgsqlHasComment("Email address of the user who committed the event.");

                entity.Property(e => e.CommitterName)
                    .IsRequired()
                    .HasColumnName("committer_name")
                    .ForNpgsqlHasComment("Name of the user who committed the event.");

                entity.Property(e => e.Conflicts)
                    .IsRequired()
                    .HasColumnName("conflicts")
                    .HasDefaultValueSql("'{}'::text[]")
                    .ForNpgsqlHasComment("Array of the names of conflicting changes.");

                entity.Property(e => e.Event)
                    .IsRequired()
                    .HasColumnName("event")
                    .ForNpgsqlHasComment("Type of event.");

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnName("note")
                    .HasDefaultValueSql("''::text")
                    .ForNpgsqlHasComment("Description of the change.");

                entity.Property(e => e.PlannedAt)
                    .HasColumnName("planned_at")
                    .HasColumnType("timestamp with time zone")
                    .ForNpgsqlHasComment("Date the event was added to the plan.");

                entity.Property(e => e.PlannerEmail)
                    .IsRequired()
                    .HasColumnName("planner_email")
                    .ForNpgsqlHasComment("Email address of the user who plan planned the change.");

                entity.Property(e => e.PlannerName)
                    .IsRequired()
                    .HasColumnName("planner_name")
                    .ForNpgsqlHasComment("Name of the user who planed the change.");

                entity.Property(e => e.Project)
                    .IsRequired()
                    .HasColumnName("project")
                    .ForNpgsqlHasComment("Name of the Sqitch project to which the change belongs.");

                entity.Property(e => e.Requires)
                    .IsRequired()
                    .HasColumnName("requires")
                    .HasDefaultValueSql("'{}'::text[]")
                    .ForNpgsqlHasComment("Array of the names of required changes.");

                entity.Property(e => e.Tags)
                    .IsRequired()
                    .HasColumnName("tags")
                    .HasDefaultValueSql("'{}'::text[]")
                    .ForNpgsqlHasComment("Tags associated with the change.");

                entity.HasOne(d => d.ProjectNavigation)
                    .WithMany(p => p.Events)
                    .HasForeignKey(d => d.Project)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("events_project_fkey");
            });

            modelBuilder.Entity<HouseCode>(entity =>
            {
                entity.HasKey(e => new { e.SiteId, e.HouseCodeId });

                entity.ToTable("house_code");

                entity.Property(e => e.SiteId).HasColumnName("site_id");

                entity.Property(e => e.HouseCodeId).HasColumnName("house_code_id");

                entity.HasOne(d => d.Site)
                    .WithMany(p => p.HouseCode)
                    .HasForeignKey(d => d.SiteId)
                    .HasConstraintName("house_code_site_id_fkey");
            });

            modelBuilder.Entity<Hub>(entity =>
            {
                entity.HasKey(e => e.MacAddress);

                entity.ToTable("hub");

                entity.HasIndex(e => e.Licence)
                    .HasName("hub_licence_key")
                    .IsUnique();

                entity.Property(e => e.MacAddress).HasColumnName("mac_address");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Licence)
                    .IsRequired()
                    .HasColumnName("licence")
                    .HasColumnType("character(30)");

                entity.Property(e => e.Modified).HasColumnName("modified");

                entity.Property(e => e.SiteId).HasColumnName("site_id");

                entity.HasOne(d => d.Site)
                    .WithMany(p => p.Hub)
                    .HasForeignKey(d => d.SiteId)
                    .HasConstraintName("hub_site_id_fkey");
            });

            modelBuilder.Entity<MailingList>(entity =>
            {
                entity.ToTable("mailing_list");

                entity.ForNpgsqlHasComment("All email addresses, linked back to web user responsible for them.");

                entity.HasIndex(e => e.Email)
                    .HasName("mailing_list_email_key")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('public.mailing_list_id_seq'::text)::regclass)");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasColumnName("active")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.WebUserId).HasColumnName("web_user_id");

                entity.HasOne(d => d.WebUser)
                    .WithMany(p => p.MailingList)
                    .HasForeignKey(d => d.WebUserId)
                    .HasConstraintName("mailing_list_web_user_id_fkey");
            });

            modelBuilder.Entity<MailingListReport>(entity =>
            {
                entity.HasKey(e => new { e.MailingListId, e.ReportId });

                entity.ToTable("mailing_list_report");

                entity.Property(e => e.MailingListId).HasColumnName("mailing_list_id");

                entity.Property(e => e.ReportId).HasColumnName("report_id");

                entity.HasOne(d => d.MailingList)
                    .WithMany(p => p.MailingListReport)
                    .HasForeignKey(d => d.MailingListId)
                    .HasConstraintName("mailing_list_report_mailing_list_id_fkey");

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.MailingListReport)
                    .HasForeignKey(d => d.ReportId)
                    .HasConstraintName("mailing_list_report_report_id_fkey");
            });

            modelBuilder.Entity<Manager>(entity =>
            {
                entity.HasKey(e => e.Mac);

                entity.ToTable("manager");

                entity.ForNpgsqlHasComment(@"Provides a link between license, parent company, site details etc... and nurse call histories uploaded by ct-touch(s) owned by parent company.
");

                entity.Property(e => e.Mac)
                    .HasColumnName("mac")
                    .ValueGeneratedNever();

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('public.manager_id_seq'::text)::regclass)");

                entity.Property(e => e.LicenseValue)
                    .IsRequired()
                    .HasColumnName("license_value");

                entity.Property(e => e.ParentCustomerId).HasColumnName("parent_customer_id");

                entity.Property(e => e.ParentCustomerName).HasColumnName("parent_customer_name");

                entity.Property(e => e.SiteId).HasColumnName("site_id");

                entity.Property(e => e.SiteName)
                    .IsRequired()
                    .HasColumnName("site_name");
            });

            modelBuilder.Entity<NurseCall>(entity =>
            {
                entity.HasKey(e => new { e.SiteId, e.CallId, e.CallDate });

                entity.ToTable("nurse_call");

                entity.HasIndex(e => e.CallDate)
                    .HasName("nurse_call_call_date_idx");

                entity.HasIndex(e => e.ResponseDate)
                    .HasName("nurse_call_response_date_idx");

                entity.HasIndex(e => new { e.SiteId, e.CallTypeId })
                    .HasName("nurse_call_site_id_call_type_id_idx");

                entity.HasIndex(e => new { e.SiteId, e.CallDate, e.ResponseDate })
                    .HasName("nurse_call_site_id_call_date_response_date_idx");

                entity.Property(e => e.SiteId).HasColumnName("site_id");

                entity.Property(e => e.CallId).HasColumnName("call_id");

                entity.Property(e => e.CallDate).HasColumnName("call_date");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.CallAccepted).HasColumnName("call_accepted");

                entity.Property(e => e.CallAcceptedCount).HasColumnName("call_accepted_count");

                entity.Property(e => e.CallTypeId).HasColumnName("call_type_id");

                entity.Property(e => e.CallerId).HasColumnName("caller_id");

                entity.Property(e => e.DayNightMode)
                    .HasColumnName("day_night_mode")
                    .HasColumnType("character varying");

                entity.Property(e => e.HouseCodeId).HasColumnName("house_code_id");

                entity.Property(e => e.Notes).HasColumnName("notes");

                entity.Property(e => e.PagerCode)
                    .HasColumnName("pager_code")
                    .HasColumnType("character varying");

                entity.Property(e => e.ResponseDate).HasColumnName("response_date");

                entity.Property(e => e.SdTransactionTimestamp).HasColumnName("sd_transaction_timestamp");

                entity.Property(e => e.SignalStrength).HasColumnName("signal_strength");

                entity.Property(e => e.TagId).HasColumnName("tag_id");

                entity.Property(e => e.TxnType).HasColumnName("txn_type");

                entity.Property(e => e.UserName)
                    .HasColumnName("user_name")
                    .HasColumnType("character varying");

                entity.Property(e => e.ZoneId).HasColumnName("zone_id");

                entity.HasOne(d => d.CallType)
                    .WithMany(p => p.NurseCall)
                    .HasForeignKey(d => d.CallTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("nurse_call_call_type_id_fkey");

                entity.HasOne(d => d.Site)
                    .WithMany(p => p.NurseCall)
                    .HasForeignKey(d => d.SiteId)
                    .HasConstraintName("nurse_call_site_id_fkey");

                entity.HasOne(d => d.HouseCode)
                    .WithMany(p => p.NurseCall)
                    .HasForeignKey(d => new { d.SiteId, d.HouseCodeId })
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("nurse_call_site_id_house_code_id_fkey");

                entity.HasOne(d => d.Tag)
                    .WithMany(p => p.NurseCall)
                    .HasForeignKey(d => new { d.SiteId, d.TagId })
                    .HasConstraintName("nurse_call_site_id_tag_id_fkey");

                entity.HasOne(d => d.Zone)
                    .WithMany(p => p.NurseCall)
                    .HasForeignKey(d => new { d.SiteId, d.ZoneId })
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("nurse_call_site_id_zone_id_fkey");

                entity.HasOne(d => d.Caller)
                    .WithMany(p => p.NurseCall)
                    .HasForeignKey(d => new { d.SiteId, d.CallerId, d.HouseCodeId })
                    .HasConstraintName("nurse_call_site_id_caller_id_house_code_id_fkey");
            });

            modelBuilder.Entity<NurseCallFileUpLoad>(entity =>
            {
                entity.HasKey(e => e.LicenseValue);

                entity.ToTable("nurse_call_file_up_load");

                entity.HasIndex(e => e.UploadedAndInHistory)
                    .HasName("added_sites")
                    .HasFilter("(uploaded_and_in_history IS TRUE)");

                entity.Property(e => e.LicenseValue)
                    .HasColumnName("license_value")
                    .HasColumnType("character(30)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('public.nurse_calls_upload_id_seq'::text)::regclass)");

                entity.Property(e => e.SiteId).HasColumnName("site_id");

                entity.Property(e => e.UploadDate).HasColumnName("upload_date");

                entity.Property(e => e.UploadedAndInHistory).HasColumnName("uploaded_and_in_history");

                entity.Property(e => e.Version).HasColumnName("version");

                entity.HasOne(d => d.LicenseValueNavigation)
                    .WithOne(p => p.NurseCallFileUpLoad)
                    .HasPrincipalKey<Hub>(p => p.Licence)
                    .HasForeignKey<NurseCallFileUpLoad>(d => d.LicenseValue)
                    .HasConstraintName("nurse_call_file_up_load_license_value_fkey");
            });

            modelBuilder.Entity<Projects>(entity =>
            {
                entity.HasKey(e => e.Project);

                entity.ToTable("projects", "sqitch");

                entity.ForNpgsqlHasComment("Sqitch projects deployed to this database.");

                entity.HasIndex(e => e.Uri)
                    .HasName("projects_uri_key")
                    .IsUnique();

                entity.Property(e => e.Project)
                    .HasColumnName("project")
                    .ValueGeneratedNever()
                    .ForNpgsqlHasComment("Unique Name of a project.");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp with time zone")
                    .HasDefaultValueSql("clock_timestamp()")
                    .ForNpgsqlHasComment("Date the project was added to the database.");

                entity.Property(e => e.CreatorEmail)
                    .IsRequired()
                    .HasColumnName("creator_email")
                    .ForNpgsqlHasComment("Email address of the user who added the project.");

                entity.Property(e => e.CreatorName)
                    .IsRequired()
                    .HasColumnName("creator_name")
                    .ForNpgsqlHasComment("Name of the user who added the project.");

                entity.Property(e => e.Uri)
                    .HasColumnName("uri")
                    .ForNpgsqlHasComment("Optional project URI");
            });

            modelBuilder.Entity<Releases>(entity =>
            {
                entity.HasKey(e => e.Version);

                entity.ToTable("releases", "sqitch");

                entity.ForNpgsqlHasComment("Sqitch registry releases.");

                entity.Property(e => e.Version)
                    .HasColumnName("version")
                    .ForNpgsqlHasComment("Version of the Sqitch registry.");

                entity.Property(e => e.InstalledAt)
                    .HasColumnName("installed_at")
                    .HasColumnType("timestamp with time zone")
                    .HasDefaultValueSql("clock_timestamp()")
                    .ForNpgsqlHasComment("Date the registry release was installed.");

                entity.Property(e => e.InstallerEmail)
                    .IsRequired()
                    .HasColumnName("installer_email")
                    .ForNpgsqlHasComment("Email address of the user who installed the registry release.");

                entity.Property(e => e.InstallerName)
                    .IsRequired()
                    .HasColumnName("installer_name")
                    .ForNpgsqlHasComment("Name of the user who installed the registry release.");
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity.ToTable("report");

                entity.ForNpgsqlHasComment("Supported report types generated for registered ct-cloud users");

                entity.HasIndex(e => e.Description)
                    .HasName("unique_rt_description")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('public.report_type_id_seq'::text)::regclass)");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasColumnName("active")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.AdminLicense)
                    .IsRequired()
                    .HasColumnName("admin_license")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.DemoLicense).HasColumnName("demo_license");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.ManagerRoleLiteLicense).HasColumnName("manager_role_lite_license");

                entity.Property(e => e.ManagerRoleProLicense)
                    .IsRequired()
                    .HasColumnName("manager_role_pro_license")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.ReportName).HasColumnName("report_name");

                entity.Property(e => e.ReportPeriodId).HasColumnName("report_period_id");

                entity.Property(e => e.ReportTypeId).HasColumnName("report_type_id");

                entity.Property(e => e.StaffRoleLiteLicense).HasColumnName("staff_role_lite_license");

                entity.Property(e => e.StaffRoleProLicense).HasColumnName("staff_role_pro_license");

                entity.HasOne(d => d.ReportPeriod)
                    .WithMany(p => p.Report)
                    .HasForeignKey(d => d.ReportPeriodId)
                    .HasConstraintName("report_report_period_id_fkey");

                entity.HasOne(d => d.ReportType)
                    .WithMany(p => p.Report)
                    .HasForeignKey(d => d.ReportTypeId)
                    .HasConstraintName("report_report_type_id_fkey");
            });

            modelBuilder.Entity<ReportPeriod>(entity =>
            {
                entity.ToTable("report_period");

                entity.ForNpgsqlHasComment("Supported license types for web application login users.");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('public.delivery_type_id_seq'::text)::regclass)");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasColumnName("active")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");
            });

            modelBuilder.Entity<ReportType>(entity =>
            {
                entity.ToTable("report_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("character varying");

                entity.Property(e => e.SiteBased).HasColumnName("site_based");
            });

            modelBuilder.Entity<ShiftPattern>(entity =>
            {
                entity.HasKey(e => new { e.ShiftId, e.TenantId });

                entity.ToTable("shift_pattern");

                entity.Property(e => e.ShiftId)
                    .HasColumnName("shift_id")
                    .HasDefaultValueSql("nextval(('public.manager_id_seq'::text)::regclass)");

                entity.Property(e => e.TenantId).HasColumnName("tenant_id");

                entity.Property(e => e.Ends)
                    .HasColumnName("ends")
                    .HasColumnType("time without time zone")
                    .HasDefaultValueSql("('now'::text)::time without time zone");

                entity.Property(e => e.Starts)
                    .HasColumnName("starts")
                    .HasColumnType("time without time zone")
                    .HasDefaultValueSql("('now'::text)::time without time zone");

                entity.HasOne(d => d.Tenant)
                    .WithMany(p => p.ShiftPattern)
                    .HasForeignKey(d => d.TenantId)
                    .HasConstraintName("shift_pattern_tenant_id_fkey");
            });

            modelBuilder.Entity<Site>(entity =>
            {
                entity.ToTable("site");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasColumnName("active")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.MajorThreshold)
                    .HasColumnName("major_threshold")
                    .HasColumnType("time without time zone")
                    .HasDefaultValueSql("'00:04:30'::time without time zone");

                entity.Property(e => e.MinorThreshold)
                    .HasColumnName("minor_threshold")
                    .HasColumnType("time without time zone")
                    .HasDefaultValueSql("'00:04:00'::time without time zone");

                entity.Property(e => e.Modified).HasColumnName("modified");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.SiteOrder).HasColumnName("site_order");

                entity.Property(e => e.TenantId).HasColumnName("tenant_id");

                entity.HasOne(d => d.Tenant)
                    .WithMany(p => p.Site)
                    .HasForeignKey(d => d.TenantId)
                    .HasConstraintName("site_tenant_id_fkey");
            });

            modelBuilder.Entity<SiteWebusers>(entity =>
            {
                entity.HasKey(e => new { e.SiteId, e.WebUserId });

                entity.ToTable("site_webusers");

                entity.Property(e => e.SiteId).HasColumnName("site_id");

                entity.Property(e => e.WebUserId).HasColumnName("web_user_id");

                entity.HasOne(d => d.Site)
                    .WithMany(p => p.SiteWebusers)
                    .HasForeignKey(d => d.SiteId)
                    .HasConstraintName("site_webusers_site_id_fkey");

                entity.HasOne(d => d.WebUser)
                    .WithMany(p => p.SiteWebusers)
                    .HasForeignKey(d => d.WebUserId)
                    .HasConstraintName("site_webusers_web_user_id_fkey");
            });

            modelBuilder.Entity<Tag>(entity =>
            {
                entity.HasKey(e => new { e.SiteId, e.TagId });

                entity.ToTable("tag");

                entity.Property(e => e.SiteId).HasColumnName("site_id");

                entity.Property(e => e.TagId).HasColumnName("tag_id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("character varying");

                entity.HasOne(d => d.Site)
                    .WithMany(p => p.Tag)
                    .HasForeignKey(d => d.SiteId)
                    .HasConstraintName("tag_site_id_fkey");
            });

            modelBuilder.Entity<Tags>(entity =>
            {
                entity.HasKey(e => e.TagId);

                entity.ToTable("tags", "sqitch");

                entity.ForNpgsqlHasComment("Tracks the tags currently applied to the database.");

                entity.HasIndex(e => new { e.Project, e.Tag })
                    .HasName("tags_project_tag_key")
                    .IsUnique();

                entity.Property(e => e.TagId)
                    .HasColumnName("tag_id")
                    .ValueGeneratedNever()
                    .ForNpgsqlHasComment("Tag primary key.");

                entity.Property(e => e.ChangeId)
                    .IsRequired()
                    .HasColumnName("change_id")
                    .ForNpgsqlHasComment("ID of last change deployed before the tag was applied.");

                entity.Property(e => e.CommittedAt)
                    .HasColumnName("committed_at")
                    .HasColumnType("timestamp with time zone")
                    .HasDefaultValueSql("clock_timestamp()")
                    .ForNpgsqlHasComment("Date the tag was applied to the database.");

                entity.Property(e => e.CommitterEmail)
                    .IsRequired()
                    .HasColumnName("committer_email")
                    .ForNpgsqlHasComment("Email address of the user who applied the tag.");

                entity.Property(e => e.CommitterName)
                    .IsRequired()
                    .HasColumnName("committer_name")
                    .ForNpgsqlHasComment("Name of the user who applied the tag.");

                entity.Property(e => e.Note)
                    .IsRequired()
                    .HasColumnName("note")
                    .HasDefaultValueSql("''::text")
                    .ForNpgsqlHasComment("Description of the tag.");

                entity.Property(e => e.PlannedAt)
                    .HasColumnName("planned_at")
                    .HasColumnType("timestamp with time zone")
                    .ForNpgsqlHasComment("Date the tag was added to the plan.");

                entity.Property(e => e.PlannerEmail)
                    .IsRequired()
                    .HasColumnName("planner_email")
                    .ForNpgsqlHasComment("Email address of the user who planned the tag.");

                entity.Property(e => e.PlannerName)
                    .IsRequired()
                    .HasColumnName("planner_name")
                    .ForNpgsqlHasComment("Name of the user who planed the tag.");

                entity.Property(e => e.Project)
                    .IsRequired()
                    .HasColumnName("project")
                    .ForNpgsqlHasComment("Name of the Sqitch project to which the tag belongs.");

                entity.Property(e => e.Tag)
                    .IsRequired()
                    .HasColumnName("tag")
                    .ForNpgsqlHasComment("Project-unique tag name.");

                entity.HasOne(d => d.Change)
                    .WithMany(p => p.Tags)
                    .HasForeignKey(d => d.ChangeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tags_change_id_fkey");

                entity.HasOne(d => d.ProjectNavigation)
                    .WithMany(p => p.Tags)
                    .HasForeignKey(d => d.Project)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tags_project_fkey");
            });

            modelBuilder.Entity<Tenant>(entity =>
            {
                entity.ToTable("tenant");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasColumnName("active")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.BreachTimeLabel)
                    .HasColumnName("breach_time_label")
                    .HasMaxLength(40)
                    .HasDefaultValueSql("'Breach'::character varying");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Modified).HasColumnName("modified");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");
            });

            modelBuilder.Entity<WeblicenseType>(entity =>
            {
                entity.ToTable("weblicense_type");

                entity.ForNpgsqlHasComment("Supported license types for web application login users.");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('public.license_type_id_seq'::text)::regclass)");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasColumnName("active")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasColumnName("label")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<WebroleType>(entity =>
            {
                entity.ToTable("webrole_type");

                entity.ForNpgsqlHasComment("Supported role types for web application login users.");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('public.role_type_id_seq'::text)::regclass)");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasColumnName("active")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasColumnName("label")
                    .HasMaxLength(40);
            });

            modelBuilder.Entity<Webusers>(entity =>
            {
                entity.ToTable("webusers");

                entity.ForNpgsqlHasComment(@"Table used to store Registered (authorised) users.
Initial design will support CTAdmin/pwd, Manager/pwd and Staff/pwd.
CTAdmin will have the rights to view ALL CT-CLOUD data, across all uploading sites, while Manager and Staff will be restricted to designated site information, with Manager also having access to more Visualization pages!");

                entity.HasIndex(e => e.Username)
                    .HasName("webusers_username_key")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval(('public.webusers_id_seq'::text)::regclass)");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasColumnName("active")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.LastLogin).HasColumnName("last_login");

                entity.Property(e => e.LicenseExpiryDate)
                    .HasColumnName("license_expiry_date")
                    .HasDefaultValueSql("(now() + '100 years'::interval)");

                entity.Property(e => e.LicenseId)
                    .HasColumnName("license_id")
                    .HasDefaultValueSql("101");

                entity.Property(e => e.Modified).HasColumnName("modified");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(40);

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .HasDefaultValueSql("502");

                entity.Property(e => e.TenantId).HasColumnName("tenant_id");

                entity.Property(e => e.TotalLogins).HasColumnName("total_logins");

                entity.Property(e => e.UserOrder).HasColumnName("user_order");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(70);

                entity.HasOne(d => d.License)
                    .WithMany(p => p.Webusers)
                    .HasForeignKey(d => d.LicenseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("webusers_license_id_fkey");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Webusers)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("webusers_role_id_fkey");

                entity.HasOne(d => d.Tenant)
                    .WithMany(p => p.Webusers)
                    .HasForeignKey(d => d.TenantId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("webusers_tenant_id_fkey");
            });

            modelBuilder.Entity<Zone>(entity =>
            {
                entity.HasKey(e => new { e.SiteId, e.ZoneId });

                entity.ToTable("zone");

                entity.Property(e => e.SiteId).HasColumnName("site_id");

                entity.Property(e => e.ZoneId).HasColumnName("zone_id");

                entity.HasOne(d => d.Site)
                    .WithMany(p => p.Zone)
                    .HasForeignKey(d => d.SiteId)
                    .HasConstraintName("zone_site_id_fkey");
            });

            modelBuilder.HasSequence("breach_id_seq");

            modelBuilder.HasSequence("delivery_type_id_seq");

            modelBuilder.HasSequence("license_type_id_seq").StartsAt(100);

            modelBuilder.HasSequence("mailing_list_id_seq");

            modelBuilder.HasSequence("manager_id_seq");

            modelBuilder.HasSequence("newcustomer_id_seq").StartsAt(62);

            modelBuilder.HasSequence("nurse_calls_upload_id_seq");

            modelBuilder.HasSequence("report_send_manager_id_seq");

            modelBuilder.HasSequence("report_type_id_seq");

            modelBuilder.HasSequence("role_type_id_seq").StartsAt(500);

            modelBuilder.HasSequence("webusers_id_seq");
        }
    }
}
