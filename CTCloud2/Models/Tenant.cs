﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Tenant
    {
        public Tenant()
        {
            AccountPeriod = new HashSet<AccountPeriod>();
            ShiftPattern = new HashSet<ShiftPattern>();
            Site = new HashSet<Site>();
            Webusers = new HashSet<Webusers>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public string BreachTimeLabel { get; set; }

        public ICollection<AccountPeriod> AccountPeriod { get; set; }
        public ICollection<ShiftPattern> ShiftPattern { get; set; }
        public ICollection<Site> Site { get; set; }
        public ICollection<Webusers> Webusers { get; set; }
    }
}
