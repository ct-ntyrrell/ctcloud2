﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Changes
    {
        public Changes()
        {
            DependenciesChange = new HashSet<Dependencies>();
            DependenciesDependencyNavigation = new HashSet<Dependencies>();
            Tags = new HashSet<Tags>();
        }

        public string ChangeId { get; set; }
        public string ScriptHash { get; set; }
        public string Change { get; set; }
        public string Project { get; set; }
        public string Note { get; set; }
        public DateTime CommittedAt { get; set; }
        public string CommitterName { get; set; }
        public string CommitterEmail { get; set; }
        public DateTime PlannedAt { get; set; }
        public string PlannerName { get; set; }
        public string PlannerEmail { get; set; }

        public Projects ProjectNavigation { get; set; }
        public ICollection<Dependencies> DependenciesChange { get; set; }
        public ICollection<Dependencies> DependenciesDependencyNavigation { get; set; }
        public ICollection<Tags> Tags { get; set; }
    }
}
