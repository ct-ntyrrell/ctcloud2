﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class NurseCallFileUpLoad
    {
        public int Id { get; set; }
        public int? SiteId { get; set; }
        public string LicenseValue { get; set; }
        public DateTime? UploadDate { get; set; }
        public bool UploadedAndInHistory { get; set; }
        public string Version { get; set; }

        public Hub LicenseValueNavigation { get; set; }
    }
}
