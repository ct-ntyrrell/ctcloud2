﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class AccountPeriod
    {
        public int PeriodId { get; set; }
        public DateTime Starts { get; set; }
        public DateTime Ends { get; set; }
        public DateTime Deadline { get; set; }
        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }
    }
}
