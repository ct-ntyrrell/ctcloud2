﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Webusers
    {
        public Webusers()
        {
            MailingList = new HashSet<MailingList>();
            SiteWebusers = new HashSet<SiteWebusers>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public long TotalLogins { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime Created { get; set; }
        public int RoleId { get; set; }
        public int LicenseId { get; set; }
        public DateTime LicenseExpiryDate { get; set; }
        public bool? Active { get; set; }
        public DateTime? Modified { get; set; }
        public int? TenantId { get; set; }
        public int? UserOrder { get; set; }

        public WeblicenseType License { get; set; }
        public WebroleType Role { get; set; }
        public Tenant Tenant { get; set; }
        public ICollection<MailingList> MailingList { get; set; }
        public ICollection<SiteWebusers> SiteWebusers { get; set; }
    }
}
