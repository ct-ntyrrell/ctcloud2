﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Projects
    {
        public Projects()
        {
            Changes = new HashSet<Changes>();
            Events = new HashSet<Events>();
            Tags = new HashSet<Tags>();
        }

        public string Project { get; set; }
        public string Uri { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatorName { get; set; }
        public string CreatorEmail { get; set; }

        public ICollection<Changes> Changes { get; set; }
        public ICollection<Events> Events { get; set; }
        public ICollection<Tags> Tags { get; set; }
    }
}
