﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class MailingList
    {
        public MailingList()
        {
            MailingListReport = new HashSet<MailingListReport>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public bool? Active { get; set; }
        public int WebUserId { get; set; }

        public Webusers WebUser { get; set; }
        public ICollection<MailingListReport> MailingListReport { get; set; }
    }
}
