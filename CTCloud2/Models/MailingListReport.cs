﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class MailingListReport
    {
        public int MailingListId { get; set; }
        public int ReportId { get; set; }

        public MailingList MailingList { get; set; }
        public Report Report { get; set; }
    }
}
