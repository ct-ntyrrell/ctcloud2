﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class ReportType
    {
        public ReportType()
        {
            Report = new HashSet<Report>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public bool SiteBased { get; set; }

        public ICollection<Report> Report { get; set; }
    }
}
