﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class HouseCode
    {
        public HouseCode()
        {
            Caller = new HashSet<Caller>();
            NurseCall = new HashSet<NurseCall>();
        }

        public int SiteId { get; set; }
        public int HouseCodeId { get; set; }

        public Site Site { get; set; }
        public ICollection<Caller> Caller { get; set; }
        public ICollection<NurseCall> NurseCall { get; set; }
    }
}
