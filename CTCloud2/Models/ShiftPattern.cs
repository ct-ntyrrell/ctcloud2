﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class ShiftPattern
    {
        public int ShiftId { get; set; }
        public TimeSpan Starts { get; set; }
        public TimeSpan Ends { get; set; }
        public int TenantId { get; set; }

        public Tenant Tenant { get; set; }
    }
}
