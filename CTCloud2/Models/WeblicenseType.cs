﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class WeblicenseType
    {
        public WeblicenseType()
        {
            Webusers = new HashSet<Webusers>();
        }

        public int Id { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public bool? Active { get; set; }

        public ICollection<Webusers> Webusers { get; set; }
    }
}
