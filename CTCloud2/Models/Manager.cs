﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Manager
    {
        public int Id { get; set; }
        public string LicenseValue { get; set; }
        public int? ParentCustomerId { get; set; }
        public string ParentCustomerName { get; set; }
        public string SiteName { get; set; }
        public int SiteId { get; set; }
        public DateTime Created { get; set; }
        public string Mac { get; set; }
        public string Description { get; set; }
    }
}
