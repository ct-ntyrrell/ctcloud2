﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class NurseCall
    {
        public int SiteId { get; set; }
        public int CallId { get; set; }
        public DateTime CallDate { get; set; }
        public DateTime? ResponseDate { get; set; }
        public int CallTypeId { get; set; }
        public bool? Active { get; set; }
        public int? CallerId { get; set; }
        public int? TagId { get; set; }
        public int? ZoneId { get; set; }
        public int? HouseCodeId { get; set; }
        public string Notes { get; set; }
        public string PagerCode { get; set; }
        public DateTime? CallAccepted { get; set; }
        public string DayNightMode { get; set; }
        public int? SignalStrength { get; set; }
        public int? TxnType { get; set; }
        public string SdTransactionTimestamp { get; set; }
        public int? CallAcceptedCount { get; set; }
        public string UserName { get; set; }

        public CallType CallType { get; set; }
        public Caller Caller { get; set; }
        public HouseCode HouseCode { get; set; }
        public Site Site { get; set; }
        public Tag Tag { get; set; }
        public Zone Zone { get; set; }
    }
}
