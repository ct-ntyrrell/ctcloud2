﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Tags
    {
        public string TagId { get; set; }
        public string Tag { get; set; }
        public string Project { get; set; }
        public string ChangeId { get; set; }
        public string Note { get; set; }
        public DateTime CommittedAt { get; set; }
        public string CommitterName { get; set; }
        public string CommitterEmail { get; set; }
        public DateTime PlannedAt { get; set; }
        public string PlannerName { get; set; }
        public string PlannerEmail { get; set; }

        public Changes Change { get; set; }
        public Projects ProjectNavigation { get; set; }
    }
}
