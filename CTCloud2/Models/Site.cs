﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Site
    {
        public Site()
        {
            Caller = new HashSet<Caller>();
            HouseCode = new HashSet<HouseCode>();
            Hub = new HashSet<Hub>();
            NurseCall = new HashSet<NurseCall>();
            SiteWebusers = new HashSet<SiteWebusers>();
            Tag = new HashSet<Tag>();
            Zone = new HashSet<Zone>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public string Description { get; set; }
        public int TenantId { get; set; }
        public bool? Active { get; set; }
        public DateTime? Modified { get; set; }
        public TimeSpan MinorThreshold { get; set; }
        public TimeSpan MajorThreshold { get; set; }
        public int? SiteOrder { get; set; }

        public Tenant Tenant { get; set; }
        public ICollection<Caller> Caller { get; set; }
        public ICollection<HouseCode> HouseCode { get; set; }
        public ICollection<Hub> Hub { get; set; }
        public ICollection<NurseCall> NurseCall { get; set; }
        public ICollection<SiteWebusers> SiteWebusers { get; set; }
        public ICollection<Tag> Tag { get; set; }
        public ICollection<Zone> Zone { get; set; }
    }
}
