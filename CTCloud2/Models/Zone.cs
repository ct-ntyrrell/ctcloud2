﻿using System;
using System.Collections.Generic;

namespace CTCloud2.Models
{
    public partial class Zone
    {
        public Zone()
        {
            NurseCall = new HashSet<NurseCall>();
        }

        public int SiteId { get; set; }
        public int ZoneId { get; set; }

        public Site Site { get; set; }
        public ICollection<NurseCall> NurseCall { get; set; }
    }
}
